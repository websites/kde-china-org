---
layout: post
title: "网站重置"
date: 2020-01-06 00:00:00
---

KDE 中国网站现在开始由 KDE WWW 托管并使用 KDE 的官方 Jekyll 模板构建。代码仓库位于 [KDE Invent](https://invent.kde.org/websites/kde-china-org/)。如需添加内容或发表文章，请提交 PR！

感谢翟翔同学长期以来对 KDE 中国网站的维护工作。感谢 Carl Schwan 和 Ben Cooksley 协助迁移。
