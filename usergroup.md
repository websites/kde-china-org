---
layout: page
title: 用户群组
konqi: /assets/img/konqi-mail.png
sorted: 4
---

# 用户群组

## 微信群

请添加管理员 guoyunhebrave 为好友并备注说明“KDE 中国”，管理员会将你拉入群组。

## Telegram 群

<https://t.me/kde_cn>

## IRC 频道

- [使用客户端打开](irc://chat.freenode.net/kde-cn)
- [使用网页端打开](https://webchat.freenode.net/?channels=#kde-cn)
