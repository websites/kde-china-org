---
layout: page
title: 同步流程
konqi: /assets/img/konqi-dev.png
sorted: 1
---

# Crowdin-SVN 同步流程

KDE 官方只提供了 SVN 作为提交翻译的唯一方式。KDE 中国社区搭建了 Crowdin 在线翻译平台的集成。

源字符串流向: SVN --> Local --> Crowdin

翻译字符串流向: Crowdin --> Local --> SVN

同步流程由[郭云鹤](https://guoyunhe.me/)同学维护。

## 系统需求

1.  git
2.  svn
3.  python
4.  gettext-tools
5.  crowdin-cli (version 2)
6.  python3-Sphinx

还需要 Crowdin 项目的管理员账号以获取 API 密钥。将 `crowdin.yaml` 的 `<your_key_string>` 替换为实际的 API 密钥。

## 初始化

```sh
git clone https://github.com/KDE-China/crowdin.git
cd crowdin
./init
```

## 同步

```sh
./sync
```

### 添加新语言

比如要添加中文（香港）。

首先在 Crowdin 网站上的设置里，添加新的 Target Language 。

然后需要修改 [environment](environment) 脚本，在 \$LANGS 里添加 zh_HK 。注意这里连字符是 \_ 。然后运行

```
./init
```

最后运行翻译上传脚本：

```
./upload_translations zh-HK
```

注意语言代码里的连字符是 - 。

以后的同步流程不变。

## 常见问题解答

### Crowdin 上传/下载进度卡住

**没办法，重新跑吧**。我们的文件太多又大，如果网络不好断了，只能重来。

### SVN 提交被拒绝

**得手动解决，挺累的**。SVN 有检查脚本，如果源字符串和翻译字符串的变量或者换行不匹配，就容易被拒绝。这时你要看报错的是哪个文件哪一行，不止要手动编辑 PO 文件，还要在 Crowdin 上找到对应的字符串改了。这个事情还挺费力的，但是没有别的办法。

### 头部贡献者名单

Crowdin 不能更新头部注释内的贡献者名单，而且不能保留旧的注释。我们现在用一个脚本来将头部备份和恢复，但是 Crowdin 上的新翻译者，我们必须得手动添加到注释里面去。
