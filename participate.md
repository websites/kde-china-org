---
layout: page
title: 参与翻译
konqi: /assets/img/konqi-docbook.png
sorted: 1
---

# 欢迎参与翻译贡献！

KDE 中国社区最主要的工作就是 KDE 软件及文档的中文翻译。

当前进度 [![Crowdin](https://d322cqt584bo4o.cloudfront.net/kdeorg/localized.svg)](https://crowdin.com/project/kdeorg)

## 加入交流群

开源工作交流是要务。

[加入群组](/usergroup.html)

加入群组之后，请先介绍自己。

## 注册 Crowdin

[项目主页](https://crowdin.com/project/kdeorg)

注册后，加入团队，即可开始翻译。但是在动工之前，请先仔细阅读下面的内容，了解基本的翻译注意事项。

Crowdin 是一个在线翻译协作平台。于 2017 年由 Guo Yunhe 引入。在此之前，只有少数拥有 KDE 提交权限的开发者可以直接翻译，其他人只能发送翻译文件到邮件列表，然后等人审核。Crowdin 带来的便利有以下几点：

- 开放参与。任何人都可以注册账号，加入团队，翻译贡献。每个人的翻译记录都会被记录下来，而不会被覆盖或丢弃。贡献者之间相互审核，选择最优的翻译。已通过的翻译会被 KDE 包含，或者最新的翻译。
- 透明运作。任何操作都会记录在活动日志当中，有不当翻译或者蓄意破坏很容易被发现并撤销。同时管理员的行为也会被监督。
- 翻译存储。不止 KDE 项目的翻译存储，Crowdin 上成千上万开源项目的翻译存储都可以作为参考。使翻译更加便利。
- 机器翻译。虽然机器翻译质量参差不齐，但也是一种有益的参考。
- 分支同步。KDE 有两个主要分支，trunk 和 stable。以前我们要同时翻译两个分支，用 Lokalize 设置起来十分繁琐。而 Crowdin 可以自动将两个分支上对应的文件的翻译同步，无需重复翻译或者进行复杂的设置。
- 操作简单。不需要会使用 Subversion 和 Lokalize 等工具，不需要每次手动在邮件列表投递。

## 使用 Crowdin

请先阅读 [Crowdin 入门](https://crowdin.com/page/tour#tab_translators)和[翻译指南](/tutorial.html)再着手施工。

## 翻译认领

项目             | 译者
----------------|-------------
Kate            | Guo Yunhe
KTextEdit       | Guo Yunhe
KDevelop        | Guo Yunhe
Krita           | Tyson Tan
